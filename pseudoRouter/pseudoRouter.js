const Router = function() {
    this.paths = {}
    this.bind = (path, type, callback) => {
        this.paths[path+type] = callback
    }
    this.runRequest = (path, type) => {
        if (this.paths[path+type]) {
            return this.paths[path+type]()
        } else {
            return 'Error 404: Not Found'
        }
    }
};

module.exports = Router;
